import { Component, OnInit } from '@angular/core';
import { Subscription, timer } from 'rxjs';

import { BackService } from './back.service';
import { DonneesDuCompteur } from './model/donnees-du-compteur';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  donneesDuCompteur: DonneesDuCompteur;
  ouvertureIode = new Date(2017, 7, 15);
  leBackRepond = true;
  nouveauRecord = false;
  retry = 0;
  timer$: Subscription;

  constructor(private backService: BackService) {}

  ngOnInit() {
    this.timer$ = timer(0, 1000 * 30).subscribe(val =>
      this.refreshDonneesDuCompteur()
    );
  }

  refreshDonneesDuCompteur() {
    this.backService.getDonneesDuCompteur().subscribe(
      response => {
        this.retry = 0;
        this.donneesDuCompteur = response;
        if (
          this.donneesDuCompteur.nbHuitresAjourdhui >
          this.donneesDuCompteur.meilleurJour.nbHuitres
        ) {
          this.nouveauRecord = true;
        }
      },
      () => {
        // si la requête au back échoue on rééssaie retryMax fois
        const retryMax = 5;
        this.retry += 1;
        console.log(new Date().toLocaleString() + ' échec n°' + this.retry + '/' + retryMax);
        if (this.retry === retryMax) {
          this.timer$.unsubscribe();
          this.leBackRepond = false;
        }
      }
    );
  }
}
