import { OnDestroy, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'age',
  pure: false
})
export class AgePipe implements PipeTransform, OnDestroy {
  constructor() {}
  transform(d: Date) {
    const now = new Date();
    now.setHours(0, 0, 0, 0);
    const seconds = Math.round(Math.abs((now.getTime() - d.getTime()) / 1000));
    const minutes = Math.round(Math.abs(seconds / 60));
    const hours = Math.round(Math.abs(minutes / 60));
    const days = Math.round(Math.abs(hours / 24));
    const months = Math.floor(Math.abs(days / 30.416));
    const years = Math.floor(Math.abs(months / 12));
    const monthsYears = months - years * 12;
    if (monthsYears === 0) {
      if (years === 1) {
        return '1 an';
      } else {
        return years + ' ans';
      }
    } else {
      if (years === 1) {
        return '1 an et ' + monthsYears + ' mois';
      } else {
        return years + ' ans et ' + monthsYears + ' mois';
      }
    }
  }
  ngOnDestroy(): void {}
}
