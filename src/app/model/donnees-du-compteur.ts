import { HuitresVendus } from './huitres-vendus';

export class DonneesDuCompteur {
    id: number;
    date: Date;
    nbHuitresDepuisLeDebut: number;
    nbHuitresAjourdhui: number;
    meilleurJour: HuitresVendus;
}
