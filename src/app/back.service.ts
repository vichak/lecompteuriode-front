import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DonneesDuCompteur } from './model/donnees-du-compteur';

@Injectable({
  providedIn: 'root'
})
export class BackService {

  constructor(private http: HttpClient) { }

  getDonneesDuCompteur(): Observable<DonneesDuCompteur> {
    return this.http.get<DonneesDuCompteur>('/api/getDonneesDuCompteur');
  }
}
